from connection_manager import connection_manager
from queue_manager import queue_manager
from logger import logger
from threading import Thread
from PyInquirer import prompt
import subprocess
import os

def open_tournament():
    input('Press enter to start the tournament. (Once started, no more player can join)\n')
    logger.print('Starting matchmaking for ' + str(connection_manager.conn_counts) + ' players')

    queue_manager.tournament_started = True

    while queue_manager.start_game():
        pass

    if connection_manager.conn_counts < 2:
        logger.print('Too little number of players')
        os._exit(1)


def start_ai_client(difficulty):
    from server import serverIp, serverPort
    engine_path = os.path.join(os.path.dirname(__file__), '../ai/src/game_engine.py')
    args = [engine_path, serverIp, serverPort, difficulty]
    try:
        subprocess.call(['python3'] + args)
    except:
        pass


def ai_difficulty_menu():
    questions = [
        {
            'type': 'list',
            'name': 'difficulty_level',
            'message': 'Please select AI difficulty:',
            'choices': [
                {
                    'name': 'Easy',
                    'value': 'easy'
                },
                {
                    'name': 'Medium',
                    'value': 'medium'
                },
                {
                    'name': 'Hard',
                    'value': 'hard'
                }
            ]
        }
    ]

    answers = prompt(questions)
    difficulty_level = answers['difficulty_level']

    logger.print('You have selected ' + difficulty_level + ' level')

    return difficulty_level


def main_menu():
    questions = [
        {
            'type': 'list',
            'name': 'mode',
            'message': 'Please select server mode:',
            'choices': [
                {
                    'name': 'Play against other player',
                    'value': 'play-against-player',
                },
                {
                    'name': 'Play against AI',
                    'value': 'ai'
                },
                {
                    'name': 'Tournament',
                    'value': 'tournament'
                }
            ]
        }
    ]

    answers = prompt(questions)
    mode = answers['mode']

    logger.print('You have selected ' + mode + ' mode')

    if mode == 'ai':
        difficulty = ai_difficulty_menu()
        mode = mode + ':' + difficulty
        Thread(target=start_ai_client, args=(difficulty,)).start()
    elif (mode == 'tournament'): Thread(target=open_tournament, args=()).start()

    return mode
