from datetime import datetime


class Logger:
    def print(self, message):
        timestamp_str = datetime.now().strftime('[%m/%d %H:%M:%S.%f]')[:-3] + ']'
        print(timestamp_str, message)


logger = Logger()
