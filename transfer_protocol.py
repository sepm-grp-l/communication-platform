from logger import logger


def send_data(conn, message, destination_description=None, log_message_content=True):
    logger.print(
        ('sending: ' + message if log_message_content else 'sending data') +
        (' to ' + destination_description if destination_description else '')
    )
    return conn.sendall(str.encode(str(message)))


def receive_data(conn):
    return conn.recv(1024 * 4).decode()
