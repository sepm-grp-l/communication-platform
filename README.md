# COMMUNICATION PLATFORM #
This repository contains the source code of a communication platform that is a part of a greater system for an online
tabletop game that allows players to connect to the same server and play a game together.

The server host is able to select one of provided sever modes:
- start a tournament (up to 8 players)
- start 1-1 game
- start a game against an AI
 
The communication platform consists of a server with five managers that handle the communication between users by
transferring data between the any two clients (players).

See the full description of component with all the requirement, constraints, diagrams, etc., [here](/docs/SEPM_D3.pdf)

### Server configuration
You can modify an ip address and the port of socket, that the server will use for communication with clients, by modifying .env file

### Setup and run
To start a server you must have [Python 3.8 (or later)](https://www.python.org/downloads/) installed.
From the project directory run the following commands in terminal:

```
pip3 install -r requirements.txt
python3 server.py
```

### Test client
There is a client prototype provided to demonstrate features of communication platform.
It doesn't rely on rules of any game it is devoted to check how the communication platform handles new connection, incoming messages, tournament queue, etc.

_NOTE: test client supports only one message sent per game, thus don't forget to send a message that ends the game
from one of the clients (i.e, 'draw', 'winner', 'loser')._

To start the client from the project directory run the following command in terminal:
```
python3 test_client.py
```

---
The component was developed by Group L (SEPM 1DL251-H21-11007, Uppsala University)