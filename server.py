import os
from dotenv import load_dotenv
import traceback
import socket
from _thread import *
import uuid
from connection_manager import connection_manager
from queue_manager import queue_manager
from transfer_protocol import send_data
from menu import main_menu
from logger import logger

load_dotenv()

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serverIp = os.getenv('SERVER_IP')
serverPort = os.getenv('SERVER_PORT')


def main():
    os.system('cls' if os.name=='nt' else 'clear')
    
    try:
        server.bind((serverIp, int(serverPort)))
    except socket.error as e:
        str(e)

    queue_manager.mode = main_menu()

    server.listen(2)
    logger.print('Server started on ' + serverIp + ':' + serverPort + '. Listening for client conns...')

    max_conn_counts = 0
    if queue_manager.mode == 'tournament':
        max_conn_counts = 8
    elif queue_manager.mode == 'play-against-player' or queue_manager.mode[:2] == 'ai':
        max_conn_counts = 2

    while True:
        try:
            # Accept client
            conn, addr = server.accept()

            if queue_manager.tournament_started:
                logger.print('Tournament has already started. Rejecting new connection')
                send_data(conn, 'tournament_started', 'rejected client')
                conn.close()
                continue

            if connection_manager.conn_counts >= max_conn_counts:
                logger.print('Server is full :(')
                send_data(conn, 'server_full', 'rejected client')
                conn.close()
                continue

            connection_manager.increment_conn_counts()

            logger.print('Conn accepted from: ' + str(addr))

            # Handle player connection
            start_new_thread(connection_manager.handle_conn, (conn, addr, str(uuid.uuid1())))
        except Exception:
            traceback.print_exc()
            break


if __name__ == '__main__':
    main()
